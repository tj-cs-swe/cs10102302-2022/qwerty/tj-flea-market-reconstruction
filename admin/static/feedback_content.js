var feedback_id = "{{ feedback_id|safe }}"; // 如果有python传过来的
    var debug = 0;

    function feedback_reply_submit() {
        let reply_url = "/api/reply_feedback";
        if (debug) reply_url = `http://127.0.0.1:4523/mock/927026${reply_url}`;
        $.ajax({
            url: reply_url,
            type: "put",
            data: JSON.stringify({ feedback_id: feedback_id, reply_content: $(feedback_reply_text).val() }),
            contentType: "application/json;charset=utf-8",
            processData: false,
            success: function (reply_ret) {
                if (reply_ret.statusCode == 200) {
                    suspend("alert-success", "回复成功: " + reply_ret.message);
                    sleep(500).then(() => {
                        window.location.href = "{{url_for('admin.feedback_show')}}";
                    });
                } else {
                    suspend("alert-danger", "回复失败: " + reply_ret.message);
                }
            },
        });
    }

    $(document).ready(function () {
        let url = "/api/admin_get_report";
        if (debug) url = `http://127.0.0.1:4523/mock/927026${url}`;
        $.ajax({
            url: url,
            type: "get",
            data: { feedback_id: feedback_id },
            success: function (ret) {
                let kind2str = { 0: "举报用户", 1: "举报物品", 2: "反馈网站bug", 3: "个人提问", 4: "交易维权", 5: "其他" };
                let state2str = { 0: "未读 | 未回复", 1: "已读 | 未回复", "-1": "已读 | 已回复" };
                $('#feedback_user_id').append(ret.data.user_id);
                $('#feedback_type').append(kind2str[ret.data.kind]);
                $('#feedback_time').append(ret.data.publish_time);
                $('#feedback_content').append(ret.data.feedback_content);
                $('#feedback_status').append(state2str[ret.data.state]);

                if (ret.data.state === -1) {
                    $('#feedback_reply_old').append(ret.data.reply_content);
                    $('#feedback_reply_new').append("更新回复")
                }
                else {
                    $('#feedback_reply_old').append("*该反馈未被任何管理员回复过*");
                    $('#feedback_reply_new').append("输入回复");
                }
            },
        });
    });