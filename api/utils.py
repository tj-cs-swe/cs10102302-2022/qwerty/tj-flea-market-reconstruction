# 返回值规范
from typing import Dict, List
# flask
from flask_socketio import emit, join_room, leave_room
from flask_login import current_user, login_user, logout_user, login_required
from flask import make_response, request, jsonify, render_template, flash, redirect, url_for
# enum
from user.models import User_state
from admin.models import Feedback_kind, Feedback_state
from order.models import Order_state
# model
from user.models import User
from admin.models import Feedback, User_Management
from order.models import Contact, Review, Order, Order_State_Item, Order_Item
from item.models import Item, History, Favor, Item_type, Item_state
from chat.models import Room, Message, Recent_Chat_List, Meet_List

from chat.routes import create_or_update_meet_list
# common
import copy
import os
import re
import json
import datetime
import time
import random
from werkzeug.security import generate_password_hash

'''
statusCode:
•	200：操作成功返回。
•	201：表示创建成功，POST 添加数据成功后必须返回此状态码。
•	400：请求格式不对。
•	401：未授权。（User/Admin）
•	404：请求的资源未找到。
•	500：内部程序错误。

其他详见接口文档
'''
SYS_ADMIN_NO = 80000000

default_res = {'success': True, 'statusCode': 200, 'message': '', 'data': {}}


def search_meet_list_by_user(user: str):
    """
    通过用户id找会话列表
    :param user: 拥有该会话列表的用户
    :return: 会话列表meet_list
    """
    meet = Meet_List.get_or_none(Meet_List.user_id == user)
    if meet is None:
        meet_list = ""
    else:
        meet_list = meet.meet_list[user]
    return meet_list


def search_room_by_sender_and_receiver(sender: str, receiver: str):
    """
    通过发送者和接收者找房间
    :param sender: 发送者
    :param receiver: 接收者
    :return: 对应房间room和房间号roomid
    """
    roomid = sender + '-' + receiver
    reroomid = receiver + '-' + sender
    room = Room.get_or_none(Room.room_id == roomid)
    reroom = Room.get_or_none(Room.room_id == reroomid)
    if room is None and reroom is None:
        room = Room.create(room_id=roomid, last_sender_id=sender)
    elif reroom is not None:
        room = reroom
        roomid = reroomid
    return room, roomid


def update_meet_list(sender: str, receiver: str, operation: str = "add" or "del"):
    """
    更新用户的会话列表
    :param sender: 会话列表所有者
    :param receiver: 从会话列表中删除/添加的另一名用户
    :param operation: 删除/添加
    """
    user, created = Meet_List.get_or_create(user_id=sender)
    meet_list = {}
    if created:
        meet_list[user] = []
    else:
        meet_list = user.meet_list
        if operation == "del":
            meet_list[sender].remove(receiver)
        else:
            meet_list[sender].append(receiver)
    Meet_List.update(meet_list=meet_list).where(Meet_List.user_id == sender).execute()


def make_response_json(statusCode: int = 200,
                       message: str = "",
                       data: any = None,
                       success: bool = None,
                       quick_response: list = None):
    """
    以统一格式包装HTTP应答消息
    :param statusCode: 应答状态码
    :param message: 应答信息
    :param data: 应答数据
    :param success: 操作是否成功，成功为True
    :param quick_response: [statusCode（若为0，则自动改为200）, message]
    如果success未指定，则当statusCode==200时为True，否则False
    """
    if data is None:
        data = {}
    if type(quick_response) == list and len(quick_response) == 2:
        statusCode = quick_response[0]
        if statusCode == 0:
            statusCode = 200
        message = quick_response[1]
    if success is None:
        success = True if statusCode // 100 == 2 else False
    return make_response(jsonify({'success': success, 'statusCode': statusCode, 'message': message, 'data': data}))


def send_message(sender: str | int, receiver: str | int, message: str, type: int = 0):
    """
    直接从后端发送消息
    :param sender: 消息发送者id
    :param receiver: 消息接收者id
    :param message: 消息内容
    :param type:消息类型，0为文本,1为图片
    """
    try:
        sender = str(sender)
        receiver = str(receiver)

        create_or_update_meet_list(sender, receiver)
        create_or_update_meet_list(receiver, sender)

        time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        room, roomid = search_room_by_sender_and_receiver(sender, receiver)
        state = Room.get_or_none(Room.room_id == roomid)

        read = 0
        if state is None:
            pass
        elif state.room_state == 2:
            read = 1

        if read == 0:
            if Recent_Chat_List.get_or_none(receiver_id=receiver) is None:
                Recent_Chat_List.insert(receiver_id=receiver,
                                        sender_id=sender,
                                        last_time=time,
                                        last_msg=message,
                                        unread=1).execute()
            else:
                Recent_Chat_List.update(
                    last_time=time,
                    last_msg=message,
                    unread=Recent_Chat_List.unread + 1).where(
                    Recent_Chat_List.receiver_id == receiver
                    and Recent_Chat_List.sender_id == sender).execute()

                Recent_Chat_List.update(
                    last_time=time,
                    last_msg=message,
                ).where(Recent_Chat_List.receiver_id == sender
                        and Recent_Chat_List.sender_id == receiver).execute()

        Message.create(msg_time=time,
                       room_id=room,
                       sender_id=sender,
                       msg_type=type,
                       msg_content=message,
                       msg_read=read)

        Room.update(
            last_message=message,
            last_sender_id=sender,
            msg_type=type).where(Room.room_id == room).execute()

        emit('message', {
            'sender': sender,
            'msg': message,
            'other_user': receiver,
            'time': time,
            'type': type
        },
             room=sender,
             namespace='/chat')

        if read == 1:
            emit('message', {
                'sender': sender,
                'msg': message,
                'other_user': sender,
                'time': time,
                'type': type
            },
                 room=receiver,
                 namespace='/chat')
        else:
            emit('notice', {
                'sender': sender,
                'msg': message,
                'other_user': sender,
                'time': time,
                'type': type
            },
                 room=receiver,
                 namespace='/chat')
        return 200, "操作成功"
    except Exception as e:
        return 500, repr(e)
